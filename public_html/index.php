<?php include 'wix-parsers/WixParser.php'; ?>
<!DOCTYPE html>
<html>
<head>
<!-- 	<?php //if(isset($_GET['domain'])) { ?>
		<script type="text/javascript">
			$.ajax({
				url: <?php //echo $_GET['domain'] ?>,

			});
		</script>
	<?php //} ?> -->
	<title>Hi, little friend</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<form action="/" class="col-md-6 col-md-offset-3 text-center">
				<h1>Parser options</h1>
				<select name="entityType" class="form-control">
					<?php foreach (WixParser::entityTypes() as $key => $value) { ?>
						<option value="<?php echo $value ?>" <?php if($_GET['entityType'] == $value) echo 'selected="selected"'; elseif ( $key = 0 && !isset($_GET['entityType']) ) echo 'selected="selected"';  {
							# code...
						} ?>><?php echo $value ?></option>
					<?php } ?>
				</select>
				<br>
				<input type="text" value="<?php if(isset($_GET['domain'])) echo $_GET['domain'] ?>" name="domain" class="form-control" placeholder="Domain for parse"/>
				<br>
				<br>
				<input type="submit" class="btn btn-success" />
			</form>
		<?php if( isset( $_GET['domain'] ) ) { ?>
		<div class="col-md-6 col-md-offset-3 text-center">
			<a href="<?php echo $_GET['domain'] = $_GET['domain'] ?>" target="_blank"><?php echo $_GET['domain'] ?></a>
		</div>
		<?php } ?>
		</div>
		<hr>
		<pre>
		<?php
			if (isset($_GET['domain']))
			{
				$wparser = new WixParser($_GET['domain']);
				switch ($_GET['entityType']) {
					case 'Posts':
						var_dump($wparser->getAllPosts(1));
						break;
					case 'Menu Structure':
						var_dump($wparser->getMenuStructure(1));
						break;
					case 'Total Menu':
						var_dump($wparser->getMenuTotal());
						break;
					case 'Test menu structure':
						var_dump($wparser->testShowMenuStructure());
						break;
					case 'Inspect Entities':
						var_dump($wparser->inspectEntities());
						break;
					case 'Posts count':
						echo $wparser->postsCount();
						break;
					case 'Menus count':
						echo $wparser->menusCount();
						break;
					case 'Get all tags':
						var_dump($wparser->getAllTags());
						break;
					default:
						echo "Unknown entity type";
						break;
				}
			}
			else {
				echo "Error: Domain name is invalid";
			}
		?>
		</pre>
	</div>
</body>
</html>
