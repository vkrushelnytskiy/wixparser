<?php include 'wix-parsers/WixParser.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Test 20 sites</title>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.js"></script>
</head>
<body>
<div class="container">
	<?php
		$sites = array(
			"http://mkristo123.wix.com/proba",
			"http://gurygar22.wix.com/prueva",
			"http://gilnasinn.wix.com/gilnas",
			"http://pro2013.wix.com/testebe",
			"http://elhousniamine.wix.com/copima",
			"http://drewparamore.wix.com/drewparamore",
			"http://swlee5.wix.com/wallasalonmedia",
			"http://ilseronteltap.wix.com/ilsetest",
			"http://misablis.wix.com/misablis",
			"http://www.ccsincus.com",
			"http://duaneht18.wix.com/pttw",
			"http://osamaalnaseer5.wix.com/alnaseer",
			"http://annabelhogan98.wix.com/nakedbeauty",
			"http://edster3194.wix.com/small-twitch-streams",
			"http://caelinkcomputers.wix.com/caelinkcomputers",
			"http://progresscomplex.wix.com/main",
			"https://leaderskw.wix.com/leaderimpact",
			"http://davids-web-design.wix.com/sierra-nevada-villa",
			"http://ferrywijsma1.wix.com/ferrynice",
			"http://www.relithrillers-tempeliers.com/"
		);

		foreach ($sites as $key => $site) {
			$wparser = new WixParser($site);
		?>
			<h1 class='text-center text-success'><?php echo $key . ". " . $site; ?></h1><div class='well'>
			<div class="panel-group" id="accordion">
				<div class="panel panel-default" id="panel1">
					<div class="panel-heading">
						<a data-toggle="collapse" data-target="#collapse<?php echo $key ?>one" href="#collapse<?php echo $key ?>one">
							Posts
						</a>
					</div>
					<div id="collapse<?php echo $key ?>one" class="panel-collapse collapse out">
					<div class="panel-body">
						<pre>
							<?php var_dump($wparser->getAllPosts()); ?>
						</pre>
					</div>

					<div class="panel-footer">
						summary
					</div>

				</div>
			</div>
			</div>

			<div class="panel-group" id="accordion">
				<div class="panel panel-warning" id="panel2">
					<div class="panel-heading">
						<a data-toggle="collapse" data-target="#collapse<?php echo $key ?>Two" href="#collapse<?php echo $key ?>Two">
							Menu
						</a>
					</div>
					<div id="collapse<?php echo $key ?>Two" class="panel-collapse collapse out">
					<div class="panel-body">
						<pre>
							<?php var_dump($wparser->getMenuTotal()); ?>
						</pre>
					</div>

					<div class="panel-footer">
						summary
					</div>

				</div>
			</div>
			</div>

			<div class="panel-group" id="accordion">
				<div class="panel panel-success" id="panel3">
					<div class="panel-heading">
						<a data-toggle="collapse" data-target="#collapse<?php echo $key ?>Three" href="#collapse<?php echo $key ?>Three">
							Tags
						</a>
					</div>
					<div id="collapse<?php echo $key ?>Three" class="panel-collapse collapse out">
					<div class="panel-body">
						<pre>
							<?php var_dump($wparser->getAllTags()); ?>
						</pre>
					</div>

					<div class="panel-footer">
						summary
					</div>

				</div>
			</div>
			</div>
			</div>

	<?php } ?>
</div>
</body>
</html>