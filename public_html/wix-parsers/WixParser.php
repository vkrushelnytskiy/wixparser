<?php
	require 'wix-orm.php';

	class WixParser {

		## Private variables for lifecycle of WixParser class

		private $domain; // The domain of Wix website
		private $url; //The URL of Wix website
		private $curl_instance; // New curl variable
		private $sitehash; // The hash of Wix website @@ Helps to find file in http://static.parastorage.com/sites/#{this hash}v=2 @@
		private $menu_total; // Menu array of Wix website @@ READY TO DATABASE WRITE @@
		private $storeId; // datastoreID of Wix Blog @@ Important variable in XHR headers @@

		## Public functions, which return answers to frontend

		public function __construct($url) {
			$this->domain = parse_url($url)['host'];
			$this->url = $url;
			$this->curl_instance = curl_init();

			$this->sitehash = $this->getSitehash();
			$this->storeId = $this->getStoreId();
		}

		public function postsCount() {
			return count( $this->getAllPosts() );
		}

		public function menusCount() {
			return count( $this->getMenuTotal() );
		}

		public function getPosts($skip) {
			$data = array(
				"getTotalCount" => false,
				"storeId" => $this->storeId,
				"collectionId" => 'Posts',
				"filter" => array("draft" => false),
				"sort" => array("date.iso" => -1),
				"skip" => $skip,
				"limit" => 10,
				"autoDereferenceLevel" => 3
			);
			curl_setopt($this->curl_instance, CURLOPT_POSTFIELDS, json_encode($data));
			return curl_exec($this->curl_instance);
		}

		public function getAllPosts($as_array = false) {
			$posts = array();
			$skip = 0;
			$end_skip = 0;
			$response = true;
			$this->initCurlInstance();

			$posts = json_decode( $this->getPosts(0 /* zero $skip gets all posts from Wix website */), 1 )['payload']['items'];

			curl_close($this->curl_instance);
			return $posts;
		}

		public function getAllTags() {
			$tags = array();
			$posts = $this->getAllPosts();

			if( count($posts) ) {
				foreach ( $posts as $key => $post ) {
					if(array_key_exists('tags', $post)) {
						foreach ( $post['tags'] as $key => $tag ) {
							$tag = array(
								'title' => $tag,
								'post_id' => intval( $post['_iid'] )
							);

							array_push($tags, $tag);
						}
					}
				}
			}

			return $tags;
		}

		public function getAllAttachments() {

		}

		public function getAllPages() {

		}

		public function getRAW() {

		}

		public function getMenuStructure($as_array = 0) {
			return json_decode(file_get_contents($this->sitehash), 1)['data']['document_data']['MAIN_MENU'];
		}

		public function inspectEntities() {
			return json_decode(file_get_contents($this->sitehash), 1);
		}

		public function getMenuTreeLoop($items, $parent='') {
			$menu_total = array();
			foreach ($items as $key => $value) {
				if ( count($value['items']) > 0) {
					$this->getMenuTreeLoop($value['items'], $value['refId']);
				}
				$cand = $this->getMenuItem($value['refId']);
				if(isset($parent))
					$cand['parent'] = $parent;
				$this->menu_total[$value['refId']] = $cand;
			}
		}

		public function getMenuTotal() {
			$this->getMenuTreeLoop($this->getMenuStructure()['items']);
			return $this->menu_total;
		}

		public function getMenuItem($item_hash) {
			return json_decode(file_get_contents($this->sitehash), 1)['data']['document_data'][$this->trim_hash($item_hash)];
		}

		public function getPostsTags($post_id) {
			return ;
		}

		public function emulateMenu() {
			$menu = $this->getMenuTotal();
			$string = "";
			for($i=0; $i<3; $i++) {
				foreach ($menu as $key => $value) {
					if(empty($value['parent'])) {
						$string .= "<li class='".self::trim_hash($key)."''>".$value['title']."<ul></ul></li>";
					} else {
						$html = str_get_html($string);
						echo ".".self::trim_hash($value['parent']);
						$xx = $html->find("li.".self::trim_hash($value['parent']));
						var_dump($xx);
						if(!empty($xx))
							$xx->innerText("<li class='".$key."'>".$value['title']."</li>");
					}
				}
			}

			return $string;
		}

		public function testShowMenuStructure() {
			foreach ($this->getMenuStructure()['items'] as $hash => $item) {
				if($this->getMenuItemChildren($item['refId'])) {
					var_dump($hash . "is parent of" . $this->getMenuItemChildren($hash));
				}
			}
		}

		## Private functions, returns answer to WixParser

		public function storeToDatabase() {
			$ORM = new WixParserORM($this);
		}

		private function getMenuItemChildren($item_hash) {
			var_dump($this->getMenuItem($item_hash));
			return $this->getMenuStructure()[$this->trim_hash($item_hash)]['items'];
		}

		private function getSitehash() {
			$main_page = file_get_contents($this->url);
			$sitehash = "";
			$jsout = strstr(strstr($main_page, 'var publicModel = '), 'static.parastorage.com/sites/');
			$position = 0;
			$str = '';
			while($str!='?') {
				$str = $jsout[$position];
				$sitehash .= $str;
				$position++;
			}
			return 'http://'.$sitehash.'v=2';
		}

		private function getStoreId() {
			$dynamicmodel = json_decode( file_get_contents($this->url . "/_api/dynamicmodel"), 1 );
			foreach ( $dynamicmodel['clientSpecMap'] as $key => $value ) {
				if( is_array($value) && array_key_exists( 'datastoreId', $value ) ) {
					return $value['datastoreId'];
				}
			}

			return NULL;
		}

		private function initCurlInstance() {
			curl_setopt_array($this->curl_instance, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $this->domain.'/apps/lists/1/Query?consistentRead=false',
				CURLOPT_POST => 1,
				CURLOPT_HTTPHEADER => array(
					"Accept:application/json",
					"Accept-Encoding:gzip,deflate",
					"Accept-Language:en-US,en;q=0.8",
					"Cache-Control:max-age=0",
					"Connection:keep-alive",
					"Content-Length:188",
					"Content-Type:application/json; charset=UTF-8"
				)
			));
		}

		## Static functions-helpers

		private static function trim_hash($hash) {
			return str_replace('#', '', $hash);
		}

		public static function entityTypes() {
			return array(
				"Posts",
				"Menu Structure",
				"Total Menu",
				"Test menu structure",
				"Inspect Entities",
				"Posts count",
				"Menus count",
				"GetPost",
				"Get all tags"
			);
		}
	}